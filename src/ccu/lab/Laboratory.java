package ccu.lab;

import java.util.HashSet;

public class Laboratory {
	
	// associations
	HashSet<Student> students = new HashSet<Student>();
	HashSet<Teacher> instructor = new HashSet<Teacher>();
	
	Integer limit;
	
	public Boolean isAvailable()
	{
		return true;
	}
	
	public Boolean canRegister(Student student)
	{
		if(students.contains(student))
			return false;
		
		return true;
	}
	
	public void register(Student student)
	{
		
	}
	
	public void increaseLimit(Integer increasement)
	{
		
	}

	// getter/setter
	public HashSet<Student> getStudents() {
		return students;
	}

	public void setStudents(HashSet<Student> students) {
		this.students = students;
	}

	public HashSet<Teacher> getInstructor() {
		return instructor;
	}

	public void setInstructor(HashSet<Teacher> instructor) {
		this.instructor = instructor;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}
}
