package ccu.lab;

import java.util.HashSet;

public class Teacher {
	
	// associations
	HashSet<Student> students = new HashSet<Student>();
	HashSet<Laboratory> laboratory = new HashSet<Laboratory>();
	
	// getter/setter
	public HashSet<Student> getStudents() {
		return students;
	}
	public void setStudents(HashSet<Student> students) {
		this.students = students;
	}
	public HashSet<Laboratory> getLaboratory() {
		return laboratory;
	}
	public void setLaboratory(HashSet<Laboratory> laboratory) {
		this.laboratory = laboratory;
	}	
}
