package ccu.lab;

import java.util.HashSet;

public class Student {

	// assocations
	HashSet<Teacher> instructor = new HashSet<Teacher>();
	HashSet<Laboratory> laboratory = new HashSet<Laboratory>();
	
	// getter/setter
	public HashSet<Teacher> getInstructor() {
		return instructor;
	}
	public void setInstructor(HashSet<Teacher> instructor) {
		this.instructor = instructor;
	}
	public HashSet<Laboratory> getLaboratory() {
		return laboratory;
	}
	public void setLaboratory(HashSet<Laboratory> laboratory) {
		this.laboratory = laboratory;
	}
}
